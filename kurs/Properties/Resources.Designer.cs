﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.19448
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace kurs.Properties {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("kurs.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap degree {
            get {
                object obj = ResourceManager.GetObject("degree", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на &lt;HTML&gt;
        ///&lt;HEAD&gt;
        ///  &lt;TITLE&gt;&lt;/TITLE&gt;
        ///&lt;/HEAD&gt;
        ///&lt;BODY&gt;
        ///&lt;P&gt;&lt;CENTER&gt;&lt;font size=4&gt;&lt;I&gt;&lt;B&gt;Кэш-фло (руб.)&lt;/B&gt;&lt;/I&gt;&lt;/font&gt;&lt;/CENTER&gt;&lt;/P&gt;
        ///&lt;table nowrap border=&quot;1&quot;&gt;
        ///&lt;tr&gt;
        ///   &lt;th height=&quot;40&quot;&gt;&amp;nbsp;&lt;font size=1&gt;&lt;B&gt;Строка&amp;nbsp;&lt;/B&gt;&lt;/font&gt;&amp;nbsp;&lt;/th&gt;
        ///   &lt;th height=&quot;40&quot;&gt;&amp;nbsp;&lt;font size=1&gt;&lt;B&gt;11.2014&amp;nbsp;&lt;/B&gt;&lt;/font&gt;&amp;nbsp;&lt;/th&gt;
        ///   &lt;th height=&quot;40&quot;&gt;&amp;nbsp;&lt;font size=1&gt;&lt;B&gt;12.2014&amp;nbsp;&lt;/B&gt;&lt;/font&gt;&amp;nbsp;&lt;/th&gt;
        ///   &lt;th height=&quot;40&quot;&gt;&amp;nbsp;&lt;font size=1&gt;&lt;B&gt;1.2015&amp;nbsp;&lt;/B&gt;&lt;/font&gt;&amp;nbsp;&lt;/th&gt;
        ///   &lt;th height=&quot;40&quot;&gt;&amp;nbsp;&lt;font size=1&gt;&lt;B&gt;2. [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string report {
            get {
                return ResourceManager.GetString("report", resourceCulture);
            }
        }
    }
}
