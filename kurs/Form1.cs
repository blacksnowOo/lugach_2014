﻿using System;
using System.Windows.Forms;

namespace kurs
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text.Equals(""))
            {
                MessageBox.Show("Введите имя таблицы");
            }
            else
            {
                Form form2 = new Form2();
                form2.Text = textBox2.Text;
                form2.ShowDialog(); 
            }
        }
    }
}
