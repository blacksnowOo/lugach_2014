﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace kurs
{
    public partial class Form2 : Form
    {
        int rowsMeter = 0;

        
        public Form2()
        {
            InitializeComponent();
            dataGridView1.RowCount = 40;
            dataGridView1.ColumnCount = CashFlow.Cf0.Length + 1;
            for (var i = 1; i <= CashFlow.Cf0.Length; i++)
            {
                dataGridView1.Columns[i].Name = CashFlow.month[i-1];
                dataGridView1.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            dataGridView1.Columns[0].Name = "Строка";
            dataGridView1.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form form3 = new Form3();
            form3.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (rowsMeter > 0)
            {
                rowsMeter--;
                for (var i = 0; i <= CashFlow.Cf0.Length; i++)
                {
                    dataGridView1.Rows[rowsMeter].Cells[i].Value = "";
                }
            }
            else
            {
                MessageBox.Show("Больше нечего удалять!");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var index0 = Data.Temp1;
            var index = Data.Temp2;
            
            dataGridView1.Rows[rowsMeter].Cells[0].Value = Data.Text;

            
            switch (Data.Global)
            {   //для простого вывода строки
                case 0:
                {
                    switch (index0)
                    {
                        case 0:
                            {   //свитч для простого вывода строки для кеш фло
                                switch (index)
                                {
                                    case 0:
                                        for (int i = 1; i <= CashFlow.Cf0.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = CashFlow.Cf0[i - 1];
                                        }
                                        break;
                                    case 1:
                                        for (int i = 1; i <= CashFlow.Cf1.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = CashFlow.Cf1[i - 1];
                                        }
                                        break;
                                    case 2:
                                        for (int i = 1; i <= CashFlow.Cf2.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = CashFlow.Cf2[i - 1];
                                        }
                                        break;
                                    case 3:
                                        for (int i = 1; i <= CashFlow.Cf3.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = CashFlow.Cf3[i - 1];
                                        }
                                        break;
                                    case 4:
                                        for (int i = 1; i <= CashFlow.Cf4.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = CashFlow.Cf4[i - 1];
                                        }
                                        break;
                                    case 5:
                                        for (int i = 1; i <= CashFlow.Cf5.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = CashFlow.Cf5[i - 1];
                                        }
                                        break;
                                    case 6:
                                        for (int i = 1; i <= CashFlow.Cf6.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = CashFlow.Cf6[i - 1];
                                        }
                                        break;
                                    case 7:
                                        for (int i = 1; i <= CashFlow.Cf7.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = CashFlow.Cf7[i - 1];
                                        }
                                        break;
                                    default:
                                        MessageBox.Show("Что-то пошло не так, с индексами кэшфло");
                                        break;
                                }
                            }
                            break;
                        case 1:
                            {   //свитч для простого вывода строки для прибылей-убытков
                                switch (index)
                                {
                                    case 0:
                                        for (int i = 1; i <= InputOutput.Io0.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = InputOutput.Io0[i - 1];
                                        }
                                        break;
                                    case 1:
                                        for (int i = 1; i <= InputOutput.Io1.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = InputOutput.Io1[i - 1];
                                        }
                                        break;
                                    case 2:
                                        for (int i = 1; i <= InputOutput.Io2.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = InputOutput.Io2[i - 1];
                                        }
                                        break;
                                    case 3:
                                        for (int i = 1; i <= InputOutput.Io3.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = InputOutput.Io3[i - 1];
                                        }
                                        break;
                                    case 4:
                                        for (int i = 1; i <= InputOutput.Io4.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = InputOutput.Io4[i - 1];
                                        }
                                        break;
                                    case 5:
                                        for (int i = 1; i <= InputOutput.Io5.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = InputOutput.Io5[i - 1];
                                        }
                                        break;
                                    case 6:
                                        for (int i = 1; i <= InputOutput.Io6.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = InputOutput.Io6[i - 1];
                                        }
                                        break;
                                    case 7:
                                        for (int i = 1; i <= InputOutput.Io7.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = InputOutput.Io7[i - 1];
                                        }
                                        break;
                                    case 8:
                                        for (int i = 1; i <= InputOutput.Io8.Length; i++)
                                        {
                                            dataGridView1.Rows[rowsMeter].Cells[i].Value = InputOutput.Io8[i - 1];
                                        }
                                        break;
                                    default:
                                        MessageBox.Show("Что-то пошло не так, с индексами прибыли-убытки");
                                        break;
                                }
                            }
                            break;
                        default:
                            MessageBox.Show("Ничего не выбранно");
                            break;
                    }    
                }
                    break;
                //тут мы делаем поиск минимального
                case 1:
                {
                    switch (index0)
                    {
                        case 0:
                            {   //свитч для поиска минимального для кеш фло
                                switch (index)
                                {
                                    case 0:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf0, CashFlow.Cf0.Min()) + 1].Value = CashFlow.Cf0.Min();
                                        break;
                                    case 1:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf1, CashFlow.Cf1.Min()) + 1].Value = CashFlow.Cf1.Min();

                                        break;
                                    case 2:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf2, CashFlow.Cf2.Min()) + 1].Value = CashFlow.Cf2.Min();

                                        break;
                                    case 3:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf3, CashFlow.Cf3.Min()) + 1].Value = CashFlow.Cf3.Min();

                                        break;
                                    case 4:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf4, CashFlow.Cf4.Min()) + 1].Value = CashFlow.Cf4.Min();

                                        break;
                                    case 5:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf5, CashFlow.Cf5.Min()) + 1].Value = CashFlow.Cf5.Min();

                                        break;
                                    case 6:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf6, CashFlow.Cf6.Min()) + 1].Value = CashFlow.Cf6.Min();

                                        break;
                                    case 7:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf7, CashFlow.Cf7.Min()) + 1].Value = CashFlow.Cf7.Min();

                                        break;
                                    default:
                                        MessageBox.Show("Что-то пошло не так, с индексами кэшфло");
                                        break;
                                }
                            }
                            break;
                        case 1:
                            {   //свитч для поиска минимального для прибылей-убытков
                                switch (index)
                                {
                                    case 0:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(InputOutput.Io0, InputOutput.Io0.Min()) + 1].Value = InputOutput.Io0.Min();

                                        break;
                                    case 1:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(InputOutput.Io1, InputOutput.Io1.Min()) + 1].Value = InputOutput.Io1.Min();

                                        break;
                                    case 2:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(InputOutput.Io2, InputOutput.Io2.Min()) + 1].Value = InputOutput.Io2.Min();

                                        break;
                                    case 3:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(InputOutput.Io3, InputOutput.Io3.Min()) + 1].Value = InputOutput.Io3.Min();

                                        break;
                                    case 4:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(InputOutput.Io4, InputOutput.Io4.Min()) + 1].Value = InputOutput.Io4.Min();

                                        break;
                                    case 5:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(InputOutput.Io5, InputOutput.Io5.Min()) + 1].Value = InputOutput.Io5.Min();

                                        break;
                                    case 6:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(InputOutput.Io6, InputOutput.Io6.Min()) + 1].Value = InputOutput.Io6.Min();

                                        break;
                                    case 7:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(InputOutput.Io7, InputOutput.Io7.Min()) + 1].Value = InputOutput.Io7.Min();

                                        break;
                                    case 8:
                                        dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(InputOutput.Io8, InputOutput.Io8.Min()) + 1].Value = InputOutput.Io8.Min();

                                        break;
                                    default:
                                        MessageBox.Show("Что-то пошло не так, с индексами прибыли-убытки");
                                        break;
                                }
                            }
                            break;
                        default:
                            MessageBox.Show("Ничего не выбранно");
                            break;
                    }
                }
                    break;
                case 2:
                {
                    //поиск максимального
                    switch (index0)
                    {
                        case 0:
                        {
                            //свитч для поиска максимально для кеш фло
                            switch (index)
                            {
                                case 0:
                                    dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf0, CashFlow.Cf0.Max()) + 1]
                                        .Value = CashFlow.Cf0.Max();
                                    break;
                                case 1:
                                    dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf1, CashFlow.Cf1.Max()) + 1]
                                        .Value = CashFlow.Cf1.Max();

                                    break;
                                case 2:
                                    dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf2, CashFlow.Cf2.Max()) + 1]
                                        .Value = CashFlow.Cf2.Max();

                                    break;
                                case 3:
                                    dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf3, CashFlow.Cf3.Max()) + 1]
                                        .Value = CashFlow.Cf3.Max();

                                    break;
                                case 4:
                                    dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf4, CashFlow.Cf4.Max()) + 1]
                                        .Value = CashFlow.Cf4.Max();

                                    break;
                                case 5:
                                    dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf5, CashFlow.Cf5.Max()) + 1]
                                        .Value = CashFlow.Cf5.Max();

                                    break;
                                case 6:
                                    dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf6, CashFlow.Cf6.Max()) + 1]
                                        .Value = CashFlow.Cf6.Max();

                                    break;
                                case 7:
                                    dataGridView1.Rows[rowsMeter].Cells[Array.IndexOf(CashFlow.Cf7, CashFlow.Cf7.Max()) + 1]
                                        .Value = CashFlow.Cf7.Max();

                                    break;
                                default:
                                    MessageBox.Show("Что-то пошло не так, с индексами кэшфло");
                                    break;
                            }
                        }
                            break;
                        case 1:
                        {
                            //свитч для поиска максимального для прибылей-убытков
                            switch (index)
                            {
                                case 0:
                                    dataGridView1.Rows[rowsMeter].Cells[
                                        Array.IndexOf(InputOutput.Io0, InputOutput.Io0.Max()) + 1].Value =
                                        InputOutput.Io0.Max();

                                    break;
                                case 1:
                                    dataGridView1.Rows[rowsMeter].Cells[
                                        Array.IndexOf(InputOutput.Io1, InputOutput.Io1.Max()) + 1].Value =
                                        InputOutput.Io1.Max();

                                    break;
                                case 2:
                                    dataGridView1.Rows[rowsMeter].Cells[
                                        Array.IndexOf(InputOutput.Io2, InputOutput.Io2.Max()) + 1].Value =
                                        InputOutput.Io2.Max();

                                    break;
                                case 3:
                                    dataGridView1.Rows[rowsMeter].Cells[
                                        Array.IndexOf(InputOutput.Io3, InputOutput.Io3.Max()) + 1].Value =
                                        InputOutput.Io3.Max();

                                    break;
                                case 4:
                                    dataGridView1.Rows[rowsMeter].Cells[
                                        Array.IndexOf(InputOutput.Io4, InputOutput.Io4.Max()) + 1].Value =
                                        InputOutput.Io4.Max();

                                    break;
                                case 5:
                                    dataGridView1.Rows[rowsMeter].Cells[
                                        Array.IndexOf(InputOutput.Io5, InputOutput.Io5.Max()) + 1].Value =
                                        InputOutput.Io5.Max();

                                    break;
                                case 6:
                                    dataGridView1.Rows[rowsMeter].Cells[
                                        Array.IndexOf(InputOutput.Io6, InputOutput.Io6.Max()) + 1].Value =
                                        InputOutput.Io6.Max();

                                    break;
                                case 7:
                                    dataGridView1.Rows[rowsMeter].Cells[
                                        Array.IndexOf(InputOutput.Io7, InputOutput.Io7.Max()) + 1].Value =
                                        InputOutput.Io7.Max();

                                    break;
                                case 8:
                                    dataGridView1.Rows[rowsMeter].Cells[
                                        Array.IndexOf(InputOutput.Io8, InputOutput.Io8.Max()) + 1].Value =
                                        InputOutput.Io8.Max();

                                    break;
                                default:
                                    MessageBox.Show("Что-то пошло не так, с индексами прибыли-убытки");
                                    break;
                            }
                        }
                            break;
                        default:
                            MessageBox.Show("Ничего не выбранно");
                            break;
                    }
                }
                    break;
                case 3:
                {
                    var temp0 = Data.Temp3;
                    var temp  = Data.Temp4;
                    double[] temps = {};
                    double[] temps1 = {}; 
                    
                    switch (index0)
                    {
                        case 0:
                            switch (index)
                            {
                                case 0:
                                {
                                    temps = CashFlow.Cf0;
                                }
                                    break;
                                case 1:
                                    {
                                        temps = CashFlow.Cf1;
                                    }
                                    break;
                                case 2:
                                    {
                                        temps = CashFlow.Cf2;
                                    }
                                    break;
                                case 3:
                                    {
                                        temps = CashFlow.Cf3;
                                    }
                                    break;
                                case 4:
                                    {
                                        temps = CashFlow.Cf4;
                                    }
                                    break;
                                case 5:
                                    {
                                        temps = CashFlow.Cf5;
                                    }
                                    break;
                                case 6:
                                    {
                                        temps = CashFlow.Cf6;
                                    }
                                    break;
                                case 7:
                                    {
                                        temps = CashFlow.Cf7;
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 1:
                            switch (index)
                            {
                                case 0:
                                {
                                    temps = InputOutput.Io0;
                                }
                                    break;
                                case 1:
                                    {
                                        temps = InputOutput.Io1;

                                    }
                                    break;
                                case 2:
                                    {
                                        temps = InputOutput.Io2;

                                    }
                                    break;
                                case 3:
                                    {
                                        temps = InputOutput.Io3;

                                    }
                                    break;
                                case 4:
                                    {
                                        temps = InputOutput.Io4;

                                    }
                                    break;
                                case 5:
                                    {
                                        temps = InputOutput.Io5;

                                    }
                                    break;
                                case 6:
                                    {
                                        temps = InputOutput.Io6;

                                    }
                                    break;
                                case 7:
                                    {
                                        temps = InputOutput.Io7;

                                    }
                                    break;
                                case 8:
                                    {
                                        temps = InputOutput.Io8;

                                    }
                                    break;
                                default:
                                    break;    
                            }
                            break;
                        default:
                            break;

                    }

                    switch (temp0)
                    {
                        case 0:
                            switch (temp)
                            {
                                case 0:
                                {
                                    temps1 = CashFlow.Cf0;
                                }
                                    break;
                                case 1:
                                    {
                                        temps1 = CashFlow.Cf1;
                                    }
                                    break;
                                case 2:
                                    {
                                        temps1 = CashFlow.Cf2;
                                    }
                                    break;
                                case 3:
                                    {
                                        temps1 = CashFlow.Cf3;
                                    }
                                    break;
                                case 4:
                                    {
                                        temps1 = CashFlow.Cf4;
                                    }
                                    break;
                                case 5:
                                    {
                                        temps1 = CashFlow.Cf5;
                                    }
                                    break;
                                case 6:
                                    {
                                        temps1 = CashFlow.Cf6;
                                    }
                                    break;
                                case 7:
                                    {
                                        temps1 = CashFlow.Cf7;
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 1:
                            switch (temp)
                            {
                                case 0:
                                    {
                                        temps1 = InputOutput.Io0;
                                    }
                                    break;
                                case 1:
                                    {
                                        temps1 = InputOutput.Io1;
                                    }
                                    break;
                                case 2:
                                    {
                                        temps1 = InputOutput.Io2;
                                    }
                                    break;
                                case 3:
                                    {
                                        temps1 = InputOutput.Io3;
                                    }
                                    break;
                                case 4:
                                    {
                                        temps1 = InputOutput.Io4;
                                    }
                                    break;
                                case 5:
                                    {
                                        temps1 = InputOutput.Io5;
                                    }
                                    break;
                                case 6:
                                    {
                                        temps1 = InputOutput.Io6;
                                    }
                                    break;
                                case 7:
                                    {
                                        temps1 = InputOutput.Io7;
                                    }
                                    break;
                                case 8:
                                    {
                                        temps1 = InputOutput.Io8;
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    for (var i = 1; i <= CashFlow.Cf0.Length; i++)
                    {
                        if (((temps[i - 1] < 0) | (temps[i - 1] > 0)) & ((temps1[i - 1] < 0) | (temps1[i - 1] > 0)))
                        {
                            dataGridView1.Rows[rowsMeter].Cells[i].Value = Math.Round(temps[i - 1] / temps1[i - 1], 2);
                        }
                        else
                            dataGridView1.Rows[rowsMeter].Cells[i].Value = 0.00;
                    }
                }
                    break;
                default:
                    MessageBox.Show("какая-то эпик ошибка");
                    break;
            }

            rowsMeter++;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application ExcelApp = new Application();
            Workbook ExcelWorkBook;
            Worksheet ExcelWorkSheet;
            //Книга.
            ExcelWorkBook = ExcelApp.Workbooks.Add(Missing.Value);
            //Таблица.
            ExcelWorkSheet = (Worksheet)ExcelWorkBook.Worksheets.get_Item(1);

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                for (int j = 0; j < dataGridView1.ColumnCount; j++)
                {
                    ExcelApp.Cells[i + 1, j + 1] = dataGridView1.Rows[i].Cells[j].Value;
                }
            }
            //Вызываем нашу созданную эксельку.
            ExcelApp.Visible = true;
            ExcelApp.UserControl = true;  
        }
    }
}
