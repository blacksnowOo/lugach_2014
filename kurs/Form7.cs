﻿using System;
using System.Windows.Forms;

namespace kurs
{
    public partial class Form7 : Form
    {
        public Form7()
        {
            InitializeComponent();
            comboBox1.Items.Add("CashFlow");
            comboBox1.Items.Add("Прибыли-убытки");
            comboBox4.Items.Add("CashFlow");
            comboBox4.Items.Add("Прибыли-убытки");
        }

        private void textBox1_TextChanged(object sender, EventArgs e){}

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var index = comboBox1.SelectedIndex;
            switch (index)
            {
                case 0:
                    comboBox2.Items.AddRange(CashFlow.Name);
                    break;
                case 1:
                    comboBox2.Items.AddRange(InputOutput.Name);
                    break;
                default:
                    MessageBox.Show("Ничего не выбранно");
                    break;
            }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            var index = comboBox4.SelectedIndex;
            switch (index)
            {
                case 0:
                    comboBox3.Items.AddRange(CashFlow.Name);
                    break;
                case 1:
                    comboBox3.Items.AddRange(InputOutput.Name);
                    break;
                default:
                    MessageBox.Show("Ничего не выбранно");
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Data.Global = 3;

            var index0 = comboBox1.SelectedIndex;
            var index =  comboBox2.SelectedIndex;

            var index1 = comboBox4.SelectedIndex;
            var index2 = comboBox3.SelectedIndex;

            if ((index >= 0) & (index <= 9))
            {
                Data.Text = "";
                Data.Text = textBox1.Text;
                Data.Temp1 = index0;
                Data.Temp2 = index;
                Data.Temp3 = index1;
                Data.Temp4 = index2;
                Close();
            }
            else
            {
                MessageBox.Show("Ничего не выбранно");
            }
        }
    }
}
