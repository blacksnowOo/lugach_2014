﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kurs
{
   
    static class Data
    {
        public static int Global { get; set; }
        public static string Text { get; set; }
        public static object Value { get; set; }
        public static int Temp1 { get; set; }
        public static int Temp2 { get; set; }
        public static int Temp3 { get; set; }
        public static int Temp4 { get; set; }
    }
    
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
