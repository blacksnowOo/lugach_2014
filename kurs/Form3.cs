﻿using System;
using System.Windows.Forms;

namespace kurs
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form form4 = new Form4();
            Form form5 = new Form5();
            Form form6 = new Form6();
            Form form7 = new Form7();
            
            if (radioButton1.Checked)
            {
                form4.ShowDialog();
            }
            if (radioButton2.Checked)
            {
                form5.ShowDialog();
            }
            if (radioButton3.Checked)
            {
                form6.ShowDialog();
            }
            if (radioButton4.Checked)
            {
                form7.ShowDialog();
            }
        }
    }
}
